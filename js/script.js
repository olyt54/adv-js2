'use strict'

//try...catch полезна в случае, если возможная ошибка не зависит от кода разработчика
//например, при получении и обработке данных с сервера или в общем при получении данных, не генерируемых разрабатываемым приложением

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

function Error(name, msg) {
    this.name = name;
    this.message = msg;
}

function addList(divId) {
    const wrapper = document.getElementById(divId);

    wrapper.append(createList(books));
}

function createList(arr) {
    const list = document.createElement('ul');
    const renderedList = [];

    arr.forEach(item => {
        try {
            if (Object.keys(item).length < 3) {
                checkProp(item);
            } else {
                renderLists(item, renderedList);
            }
        } catch (error) {
            console.error(error.name, error.message);
        }
    });

    renderedList.forEach(item => {
            list.append(item);
    })
    return list;
}

function checkProp (object) {
    const checkList = [
        'name',
        'author',
        'price'
    ];

    checkList.forEach(prop => {
        for (let key in object) {
            if (!object[prop]) {
                throw new Error('Missing property', `There is missing ${prop} property`);
            }
        }
    });
}

function renderLists (object, resultArr) {
    const listItem = document.createElement('li'),
        innerList = document.createElement('ul');

    for (let key in object) {
        const innerListItem = document.createElement('li');

        innerListItem.textContent = object[key];
        innerList.append(innerListItem);
    }

    listItem.append(innerList);
    resultArr.push(listItem);
}

addList('root');